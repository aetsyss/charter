//
//  CISViewController.h
//  charter
//
//  Created by Алексей Цысс on 24.07.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CISHTTP.h"
#import "CISGlobalData.h"
#import <CoreLocation/CoreLocation.h>

#import "CISSideMenu.h"
#import "KLCPopup.h"
#import "ECSlidingViewController/ECSlidingViewController.h"
#import "CISFlightDetailViewController.h"

@interface CISViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, ECSlidingViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *mainTable;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UIView *topView;

- (IBAction)sideMenuButtonPressed;

@end
