//
//  CISHTTP.h
//  keepConnection
//
//  Created by Алексей Цысс on 21.05.14.
//  Copyright (c) 2014 ittaxi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProgressHUD.h"

@interface CISHTTP : NSObject

+ (CISHTTP *)shared;

- (void)initWithServerURL:(NSString *)urlString;
- (void)postData:(NSDictionary *)jsonData withCommand:(NSString *)command completionHandler:(void (^)(NSDictionary *ans))myCompletionHandler;

@end
