//
//  CISFlightDetailViewController.h
//  charter
//
//  Created by Алексей Цысс on 06.08.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CISGlobalData.h"

#import "CISVariant.h"

@interface CISFlightDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSDictionary *offer;
@property (strong, nonatomic) IBOutlet UITableView *mainTable;

@end
