//
//  CISGlobalData.m
//  charter
//
//  Created by Алексей Цысс on 24.07.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import "CISGlobalData.h"

@implementation CISGlobalData

//-------------------------------------------------------------------------------------------------------------------------------------------------

+ (CISGlobalData *)shared
{
    static dispatch_once_t once = 0;
	static CISGlobalData *mainConnection;
    
    dispatch_once(&once, ^{ mainConnection = [[CISGlobalData alloc] init]; });
	
	return mainConnection;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (id)init
{
    self = [super init];
    
    if (self) {
        
        _countryList = [[NSMutableDictionary alloc] init];
        _cityFromList = [[NSMutableArray alloc] init];
        _cityToList = [[NSMutableArray alloc] init];
        _specialList = [[NSMutableArray alloc] init];
        _tariffList = [[NSMutableArray alloc] init];
        
        _selectedCityFrom = [NSMutableString stringWithString:@""];
        _selectedCityTo = [NSMutableString stringWithString:@""];
        _filterWhenString = [NSMutableString stringWithString:@"ближайшие полгода"];
        
        _passangersCount = [[NSMutableString alloc] initWithString:@"1"];
        
        NSDate *toDay = [NSDate date];
        
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setMonth:5];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *toDayPlusMonth = [calendar dateByAddingComponents:dateComponents toDate:toDay options:0];
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd.MM.yyyy"];
        
        _dateFrom = [[NSMutableString alloc] initWithString:[df stringFromDate:toDay]];
        _dateTo = [[NSMutableString alloc] initWithString:[df stringFromDate:toDayPlusMonth]];
        
        NSLog(@"GlobalData singleton created");
        
        
	}
    
	return self;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)getCountryList
{
    
    NSDictionary *geocoder_request = @{
                                       @"type": @"getCountryList"
                                       };
    
    [[CISHTTP shared] postData:geocoder_request withCommand:@"json.asp" completionHandler:^(NSDictionary *ans) {
        
        NSArray *countries = [ans valueForKey:@"country"];
        
        [_countryList removeAllObjects];
        
        for (NSDictionary *dict in countries) {
            
            NSString *countryCode = [dict valueForKey:@"code"];
            NSString *countryName = [dict valueForKey:@"name"];
            
            [_countryList setValue:countryName forKey:countryCode];
        }
        
        
        NSLog(@"countryList loaded, %lu", (unsigned long)[_countryList count]);
        
    }];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)getCityFromList
{
    
    NSDictionary *geocoder_request = @{
                                       @"type": @"getCityFromList"
                                       };
    
    [[CISHTTP shared] postData:geocoder_request withCommand:@"json.asp" completionHandler:^(NSDictionary *ans) {
        
        NSArray *list_from = [ans objectForKey:@"list_from"];
        
        [_cityFromList removeAllObjects];
        
        for (NSDictionary *dict in list_from) {
            
            NSString *country = [dict valueForKey:@"country"];
            NSString *code = [dict valueForKey:@"code"];
            
            NSArray *cities = [dict valueForKey:@"cities"];
            
            for (NSDictionary *city in cities) {
                
                NSString *cityName = [city valueForKey:@"name"];
                NSString *cityCode = [city valueForKey:@"code"];
                
                NSDictionary *save = @{@"countryName": country, @"countryCode": code, @"cityName": cityName, @"cityCode": cityCode};
                
                [_cityFromList addObject:save];
            }
            
        }
        
        NSArray *sorted = [_cityFromList sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            NSDictionary *first = (NSDictionary *)obj1;
            NSDictionary *second = (NSDictionary *)obj2;
            
            NSString *a = [first valueForKey:@"cityName"];
            NSString *b = [second valueForKey:@"cityName"];
            
            return [a compare:b];
            
        }];
        
        _cityFromList = [NSMutableArray arrayWithArray:sorted];
        
        dispatch_async(dispatch_get_main_queue(), ^{

            NSLog(@"cityFromList loaded, %lu", (unsigned long)[_cityFromList count]);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getCityFromList" object:nil];

        });

    }];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)getSchedCountryListFromCountry:(NSString *)country andFromCity:(NSString *)city
{
    
    [_cityToList removeAllObjects];
    
    if (country == nil || city == nil) {
        return;
    }
    
    NSDictionary *geocoder_request = @{
                                       @"type": @"getSсhedCountryList",
                                       @"country_from": country,
                                       @"city_from": city
                                       };
    
    [[CISHTTP shared] postData:geocoder_request withCommand:@"json.asp" completionHandler:^(NSDictionary *ans) {
        
        NSArray *list_to = [ans objectForKey:@"list_to"];
        
        for (NSDictionary *dict in list_to) {
            
            NSDictionary *country_to = [dict valueForKey:@"country_to"];
            
            NSString *country = [country_to valueForKey:@"name"];
            NSString *code = [country_to valueForKey:@"code"];
            
            NSArray *cities = [dict valueForKey:@"cities_to"];
            
            for (NSDictionary *city in cities) {
                
                NSString *cityName = [city valueForKey:@"name"];
                NSString *cityCode = [city valueForKey:@"code"];
                
                NSDictionary *save = @{@"countryName": country, @"countryCode": code, @"cityName": cityName, @"cityCode": cityCode};
                
                [_cityToList addObject:save];
            }
            
        }
        
        NSArray *sorted = [_cityToList sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            NSDictionary *first = (NSDictionary *)obj1;
            NSDictionary *second = (NSDictionary *)obj2;
            
            NSString *a = [first valueForKey:@"cityName"];
            NSString *b = [second valueForKey:@"cityName"];
            
            return [a compare:b];
            
        }];
        
        _cityToList = [NSMutableArray arrayWithArray:sorted];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"cityToList loaded, %lu", (unsigned long)[_cityToList count]);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getSсhedCountryList" object:nil];
            
        });

    }];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSDictionary *)getCityToDictionaryByCityName:(NSString *)city_name
{

    NSDictionary *ret = nil;
    
    for (NSDictionary *dict in _cityToList) {
        
        NSString *cityName = [dict valueForKey:@"cityName"];
        
        if ([city_name isEqualToString:cityName]) {
            
            ret = dict;
            break;
        }
    }
    
    return ret;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)getSpecialListFromCountry:(NSString *)country_from fromCity:(NSString *)city_from toCountry:(NSString *)country_to toCity:(NSString *)city_to
{
    
    if (country_from == nil) {
        country_from = @"";
    }
    
    if (city_from == nil) {
        city_from = @"";
    }
    
    if (country_to == nil) {
        country_to = @"";
    }
    
    if (city_to == nil) {
        city_to = @"";
    }
    
    NSDictionary *geocoder_request = @{
                                       @"type": @"getSpecialList",
                                       @"country_from": country_from,
                                       @"city_from": city_from,
                                       @"country_to": country_to,
                                       @"city_to": city_to
                                       };
    
    [[CISHTTP shared] postData:geocoder_request withCommand:@"json.asp" completionHandler:^(NSDictionary *ans) {
        
        NSArray *special = [ans valueForKey:@"special"];
        
        [_specialList removeAllObjects];
        
        [_specialList addObjectsFromArray:special];
        
        NSArray *sorted = [_specialList sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            NSDictionary *first = (NSDictionary *)obj1;
            NSDictionary *second = (NSDictionary *)obj2;
            
            NSDictionary *a = [first valueForKey:@"price"];
            NSDictionary *b = [second valueForKey:@"price"];
            
            NSString *aa = [a valueForKey:@"summ"];
            NSString *bb = [b valueForKey:@"summ"];
            
            float aaa = [aa floatValue];
            float bbb = [bb floatValue];
            
            return (aaa > bbb);
        }];
        
        _specialList = [NSMutableArray arrayWithArray:sorted];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"specialList loaded, %lu", (unsigned long)[_specialList count]);
            
            //NSLog(@"specialList: %@", _specialList);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getSpecialList" object:nil];
            
        });
        
    }];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)getTariffList
{
    
    NSString *city_from = [_cityFromData valueForKey:@"cityCode"];
    NSString *city_to = [_cityToData valueForKey:@"cityCode"];
    NSString *date_from = _dateFrom;
    NSString *date_to = _dateTo;
    
    if (city_from == nil) {
        city_from = @"";
    }
    
    if (city_to == nil) {
        city_to = @"";
    }
    
    if (date_from == nil) {
        date_from = @"";
    }
    
    if (date_to == nil) {
        date_to = @"";
    }
    
    NSDictionary *geocoder_request = @{
                                       @"type": @"getTariffList",
                                       @"city_from": city_from,
                                       @"city_to": city_to,
                                       @"tariff_type": @"rt",
                                       @"is_no_range": @1,
                                       @"is_seats": @1,
                                       @"is_tariff_range": @0,
                                       @"date_from": date_from,
                                       @"date_to": date_to,
                                       @"range_duration": @0,
                                       @"range_value": @0,
                                       @"pass_count": _passangersCount,
                                       @"rec_count": @0,
                                       @"page_number": @0
                                       };
    
    
    [[CISHTTP shared] postData:geocoder_request withCommand:@"json.asp" completionHandler:^(NSDictionary *ans) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *tariff = [ans valueForKey:@"tariff"];
            
            NSArray *sorted = [tariff sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                
                NSDictionary *first = (NSDictionary *)obj1;
                NSDictionary *second = (NSDictionary *)obj2;
                
                NSDictionary *a = [first valueForKey:@"price"];
                NSDictionary *b = [second valueForKey:@"price"];
                
                NSString *aa = [a valueForKey:@"adt"];
                NSString *bb = [b valueForKey:@"adt"];
                
                float aaa = [aa floatValue];
                float bbb = [bb floatValue];
                
                return (aaa > bbb);
            }];
            
            _tariffList = [NSMutableArray arrayWithArray:sorted];
            
            NSLog(@"tariff list loaded: %ld", [_tariffList count]);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getTariffList" object:nil];

        });
        
    }];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)rotateDate:(NSString *)date
{
    
    NSArray *arr = [date componentsSeparatedByString:@"-"];
    
    NSString *ret = [NSString stringWithFormat:@"%@.%@.%@", arr[2], arr[1], arr[0]];
    
    return ret;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

@end
