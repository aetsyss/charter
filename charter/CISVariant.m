//
//  CISVariant.m
//  charter
//
//  Created by Алексей Цысс on 07.08.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import "CISVariant.h"

@implementation CISVariant

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (id)initVariantWidthDataFromDictionary:(NSDictionary *)variant
{
    
    self = [super init];
    
    if (self) {
        
        _variantId = [variant valueForKey:@"id"];
        
        NSArray *flights = [variant valueForKey:@"flight"];
        
        _flights = [[NSMutableArray alloc] init];
        
        for (NSDictionary *flightDict in flights) {
            
            CISFlight *flight = [[CISFlight alloc] initWithFlightDictionary:flightDict];
            
            [_flights addObject:flight];
        }
        
    }
    
    return self;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------


@end
