//
//  CISVariant.h
//  charter
//
//  Created by Алексей Цысс on 07.08.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CISFlight.h"

@interface CISVariant : NSObject

@property (strong, nonatomic) NSString *variantId;
@property (strong, nonatomic) NSMutableArray *flights;

- (id)initVariantWidthDataFromDictionary:(NSDictionary *)variant;

@end
