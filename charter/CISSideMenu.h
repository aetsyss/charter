//
//  CISSideMenu.h
//  charter
//
//  Created by Алексей Цысс on 04.08.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CISSideMenu : UIView

@property (strong, nonatomic) IBOutlet UITableView *mainTable;

@end
