//
//  CISViewController.m
//  charter
//
//  Created by Алексей Цысс on 24.07.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import "CISViewController.h"

#import "UIViewController+ECSlidingViewController.h"
#import "MEDynamicTransition.h"
#import "METransitions.h"

@interface CISViewController ()
{
    NSArray *tableData;
    
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    
    NSString *geocoderDefinedCity;

}

@property (nonatomic, strong) METransitions *transitions;
@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;

@end

@implementation CISViewController

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"mainMenuDidLoad");
	// Do any additional setup after loading the view, typically from a nib.
    
    _activityIndicator.hidden = YES;
    
    self.navigationController.navigationBar.barTintColor = _topView.backgroundColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    
    self.transitions.dynamicTransition.slidingViewController = self.slidingViewController;
    
    NSDictionary *transitionData = self.transitions.all[1];
    id<ECSlidingViewControllerDelegate> transition = transitionData[@"transition"];
    if (transition == (id)[NSNull null]) {
        self.slidingViewController.delegate = nil;
    } else {
        self.slidingViewController.delegate = transition;
    }
    
    NSString *transitionName = transitionData[@"name"];
    if ([transitionName isEqualToString:METransitionNameDynamic]) {
        self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGestureCustom;
        self.slidingViewController.customAnchoredGestures = @[self.dynamicTransitionPanGesture];
        [self.navigationController.view removeGestureRecognizer:self.slidingViewController.panGesture];
        [self.navigationController.view addGestureRecognizer:self.dynamicTransitionPanGesture];
    } else {
        self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
        self.slidingViewController.customAnchoredGestures = @[];
        [self.navigationController.view removeGestureRecognizer:self.dynamicTransitionPanGesture];
        [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
    
    //////////////////////////////////////////////////////////////////////
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSpecialList) name:@"getSpecialList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTariffList) name:@"getTariffList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCityFromList) name:@"getCityFromList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityFromChanged:) name:@"cityFromChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTicketsTable) name:@"reloadTicketsTable" object:nil];

}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)viewDidAppear:(BOOL)animated
{

    NSIndexPath *sel = [_mainTable indexPathForSelectedRow];
    [_mainTable deselectRowAtIndexPath:sel animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)reloadTicketsTable
{
   
    [[CISGlobalData shared].tariffList removeAllObjects];
    [_mainTable reloadData];
    
    _activityIndicator.hidden = NO;
    [_activityIndicator startAnimating];
    
    [[CISGlobalData shared] getTariffList];
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (METransitions *)transitions
{
    
    if (_transitions) return _transitions;
    
    _transitions = [[METransitions alloc] init];
    
    return _transitions;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (UIPanGestureRecognizer *)dynamicTransitionPanGesture
{
    
    if (_dynamicTransitionPanGesture) return _dynamicTransitionPanGesture;
    
    _dynamicTransitionPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.transitions.dynamicTransition action:@selector(handlePanGesture:)];
    
    return _dynamicTransitionPanGesture;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    [locationManager stopUpdatingLocation];
    
    CLLocation *lastLocation = [locations lastObject];
    
    [geocoder reverseGeocodeLocation:lastLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if ([placemarks count] > 0) {
            
            CLPlacemark *place = [placemarks firstObject];

            NSDictionary *addressDict = place.addressDictionary;
            geocoderDefinedCity = [addressDict valueForKey:@"City"];
            [CISGlobalData shared].selectedCityFrom = [NSMutableString stringWithString:geocoderDefinedCity];
            
            NSLog(@"Ваш город: %@", geocoderDefinedCity);
            
            [[CISGlobalData shared] getCityFromList];
        
        }
        
    }];
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    NSLog(@"locationManager didFailWithError");
    
    geocoderDefinedCity = @"Москва";
    
    [[CISGlobalData shared] getCityFromList];
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)getCityFromList
{
    
    BOOL cityFound = NO;
 
    for (NSDictionary *dict in [CISGlobalData shared].cityFromList) {
        
        NSString *cityName = [dict valueForKey:@"cityName"];
        
        if ([cityName isEqualToString:geocoderDefinedCity]) {
            
            cityFound = YES;
            
            if ([CISGlobalData shared].cityFromData == nil) {
                [CISGlobalData shared].cityFromData = dict;
            }
            
            _activityIndicator.hidden = NO;
            [_activityIndicator startAnimating];
            
            [[CISGlobalData shared] getSchedCountryListFromCountry:[dict valueForKey:@"countryCode"] andFromCity:[dict valueForKey:@"cityCode"]];
            [[CISGlobalData shared] getTariffList];

        }
        
    }
    
    if (cityFound == NO) {
        
        geocoderDefinedCity = @"Москва";
        
        [[CISGlobalData shared] getCityFromList];
    }
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)getSpecialList
{
    
    tableData = [CISGlobalData shared].specialList;
    [_mainTable reloadData];
    
    if ([tableData count] == 0) {
        
        [ProgressHUD showSuccess:@"По данному направлению нет предложений"];
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)getTariffList
{
    
    [_activityIndicator stopAnimating];
    _activityIndicator.hidden = YES;
    
    tableData = [CISGlobalData shared].tariffList;
    [_mainTable reloadData];
    
    if ([tableData count] == 0) {
        
        [ProgressHUD showSuccess:@"По данному направлению нет предложений"];
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    return [tableData count];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
        
    }
    
    
    if (tableData == [CISGlobalData shared].specialList) {
        
        NSDictionary *offer = [tableData objectAtIndex:indexPath.row];

        
        NSDictionary *city_from = [offer valueForKey:@"city_from"];
        NSDictionary *city_to = [offer valueForKey:@"city_to"];
        
        UILabel *cityFromLabel = (UILabel *)[cell viewWithTag:1];
        UILabel *cityToLabel = (UILabel *)[cell viewWithTag:2];
        
        cityFromLabel.text = [city_from valueForKey:@"name"];
        cityToLabel.text = [city_to valueForKey:@"name"];
        
        ///////////////////////////////////////////////////////////////
        
        NSDictionary *price = [offer valueForKey:@"price"];
        
        UILabel *dateLabel = (UILabel *)[cell viewWithTag:3];
        UILabel *summLabel = (UILabel *)[cell viewWithTag:4];
        
        NSString *date = [price valueForKey:@"data"];
        dateLabel.text = [[CISGlobalData shared] rotateDate:date];
        summLabel.text = [NSString stringWithFormat:@"%@р.", [self priceToSeporatedString: [price valueForKey:@"summ"]]];
        
        //////////////////////////////////////////////////////////////
        
        NSString *tarifType = [offer valueForKey:@"tariff_type"];
        
        UIImageView *flightDirectionImageView = (UIImageView *)[cell viewWithTag:6];
        
        if ([tarifType isEqualToString:@"RT"]) {
            flightDirectionImageView.image = [UIImage imageNamed:@"2planes.png"];
        } else {
            flightDirectionImageView.image = [UIImage imageNamed:@"1plane.png"];
        }
    }
    
    if (tableData == [CISGlobalData shared].tariffList) {
        
        NSDictionary *tarif = [tableData objectAtIndex:indexPath.row];
        NSString *tarifType = [tarif valueForKey:@"type"];
        NSDictionary *price = [tarif valueForKey:@"price"];
        NSDictionary *legs = [tarif valueForKey:@"legs"];
        NSDictionary *dir0 = [legs valueForKeyPath:@"dir0"];
        NSArray *variants = [dir0 valueForKey:@"variant"];
        NSDictionary *firstVariantDict = [variants firstObject];
        
        CISVariant *firstVariant = [[CISVariant alloc] initVariantWidthDataFromDictionary:firstVariantDict];
        CISFlight *forwardFlight = [firstVariant.flights firstObject];
        
        ((UILabel *)[cell viewWithTag:1]).text = [[CISGlobalData shared].cityFromData valueForKey:@"cityName"]; //forwardFlight.fromName;
        ((UILabel *)[cell viewWithTag:2]).text = forwardFlight.toName;
        ((UILabel *)[cell viewWithTag:3]).text = [[CISGlobalData shared] rotateDate:forwardFlight.date];
        ((UILabel *)[cell viewWithTag:4]).text = [NSString stringWithFormat:@"%@р.", [self priceToSeporatedString: [price valueForKey:@"adt"]]];
        
        NSDictionary *cityToDict = [[CISGlobalData shared] getCityToDictionaryByCityName:forwardFlight.toName];
        ((UILabel *)[cell viewWithTag:5]).text = [cityToDict valueForKey:@"countryName"];
        
        ((UILabel *)[cell viewWithTag:6]).text = [[CISGlobalData shared].cityFromData valueForKey:@"countryName"];
        
        UIImageView *flightDirectionImageView = (UIImageView *)[cell viewWithTag:7];

        if ([tarifType isEqualToString:@"RT"]) {
            flightDirectionImageView.image = [UIImage imageNamed:@"2planes.png"];
        } else {
            flightDirectionImageView.image = [UIImage imageNamed:@"1plane.png"];
        }
    }
    
    //////////////////////////////////////////////////////////////
    
    if (indexPath.row % 2) {
        cell.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)cityFromChanged:(NSNotification *)notification
{
    
    [[CISGlobalData shared] getTariffList];

}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)priceToSeporatedString:(NSString *)str
{
    
    NSMutableString *ret = [[NSMutableString alloc] init];
    NSMutableString *cut = [NSMutableString stringWithFormat:@"%ld", (long)[str integerValue]];

    while ([cut length] > 3) {
        
        NSString *last3ch = [cut substringFromIndex:[cut length] - 3];
        ret = [NSMutableString stringWithFormat:@"%@ %@", last3ch, ret];
        
        cut = [NSMutableString stringWithString:[cut substringToIndex:[cut length] - 3]];
        
    }
    
    if ([cut length] > 0) {
        
        ret = [NSMutableString stringWithFormat:@"%@ %@", cut, ret];

    }
    
    return ret;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction)sideMenuButtonPressed
{
    
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"flightDetailsSegue"]) {
        
        NSIndexPath *sel = [_mainTable indexPathForSelectedRow];
        
        CISFlightDetailViewController *destVC = (CISFlightDetailViewController *)segue.destinationViewController;
        destVC.offer = [tableData objectAtIndex:sel.row];
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

@end
