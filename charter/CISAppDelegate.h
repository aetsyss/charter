//
//  CISAppDelegate.h
//  charter
//
//  Created by Алексей Цысс on 24.07.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CISHTTP.h"
#import "CISGlobalData.h"

@interface CISAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
