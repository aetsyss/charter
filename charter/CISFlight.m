//
//  CISFlight.m
//  charter
//
//  Created by Алексей Цысс on 07.08.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import "CISFlight.h"

@implementation CISFlight

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (id)initWithFlightDictionary:(NSDictionary *)flight
{
    self = [super init];
    
    if (self) {
        
        _date = [flight valueForKeyPath:@"date"];
        _flightNumber = [flight valueForKey:@"flnum"];
        
        NSDictionary *airline = [flight valueForKey:@"airline"];
        _airLineCode = [airline valueForKey:@"code"];
        _airLineName = [airline valueForKey:@"name"];
        
        NSDictionary *from = [flight valueForKey:@"from"];
        _fromCode = [from valueForKey:@"code"];
        _fromName = [from valueForKey:@"name"];
        
        NSDictionary *to = [flight valueForKey:@"to"];
        _toCode = [to valueForKey:@"code"];
        _toName = [to valueForKey:@"name"];
        
        /*_dateFrom = [flight valueForKey:@"date"];
        _dateFrom = [self rotateDate:_dateFrom];
        NSDate *dateFromDate = [self stringDateToNSDate:_dateFrom];
        
        NSDictionary *time = [flight valueForKey:@"time"];
        _timeFrom = [time valueForKey:@"dep"];
        _timeTo = [time valueForKey:@"arr"];
        _flightDuration = [time valueForKey:@"all"];
        
        NSNumber *arrflag = [time valueForKey:@"arrflag"];
        
        NSDate *dateToDate;
        if ([arrflag intValue] == 1) { // если прилет на следующий день
            
            dateToDate = [dateFromDate dateByAddingTimeInterval:60*60*24];
        } else {
            
            dateToDate = dateFromDate;
        }
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd.MM.yyyy"];
        
        _dateTo = [df stringFromDate:dateToDate];
        
        _dayOfWeekFrom = [self dayOfWeekFromNSDate:dateFromDate];
        _dayOfWeekTo = [self dayOfWeekFromNSDate:dateToDate];*/
        
    }
    
	return self;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)rotateDate:(NSString *)date
{
    
    NSArray *arr = [date componentsSeparatedByString:@"-"];
    
    NSString *ret = [NSString stringWithFormat:@"%@.%@.%@", arr[2], arr[1], arr[0]];
    
    return ret;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSDate *)stringDateToNSDate:(NSString *)date
{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd.MM.yyyy"];
    return [df dateFromString:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)dayOfWeekFromNSDate:(NSDate *)date
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    return [dateFormatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSDate *)fromDate
{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd.MM.yyyy hh:mm:ss"];
    
    return [df dateFromString:[NSString stringWithFormat:@"%@ %@:00", _dateFrom, _timeFrom]];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSDate *)toDate111
{
    
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"dd.MM.yyyy hh:mm:ss"];
    
    NSString *msk = [NSString stringWithFormat:@"%@ %@:00", _dateFrom, _timeFrom];
    NSDate *ret = [df1 dateFromString:msk];
    
    return ret;
    
    //return [df dateFromString:[NSString stringWithFormat:@"%@ %@:00", _dateTo, _timeTo]];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------


@end
