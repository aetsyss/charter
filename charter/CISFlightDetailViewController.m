//
//  CISFlightDetailViewController.m
//  charter
//
//  Created by Алексей Цысс on 06.08.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import "CISFlightDetailViewController.h"

@interface CISFlightDetailViewController ()
{
    
    NSString *travelPrice;
    
    //CISFlight *forwardFlight;
    //NSMutableArray *backwordFlights;
    
    //NSMutableArray *forwardVariants;
    //NSMutableArray *backwordVariants;
    NSDictionary *city_from;
    NSDictionary *city_to;
    NSDictionary *country_to;
    
    NSMutableArray *variants;
}

@end

@implementation CISFlightDetailViewController

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    NSLog(@"selected offer: %@", _offer);
    
    city_from = [_offer valueForKey:@"city_from"];
    city_to = [_offer valueForKey:@"city_to"];
    country_to = [_offer valueForKey:@"country_to"];
    
    NSDictionary *price = [_offer valueForKey:@"price"];
    travelPrice = [price valueForKey:@"summ"];
    
    
    NSDictionary *legs = [_offer valueForKey:@"legs"];
    variants = [[NSMutableArray alloc] init];
    
    // create forward flight varants (forwardVariants)
    
    NSDictionary *dir0 = [legs valueForKey:@"dir0"];
    NSArray *variants0 = [dir0 valueForKey:@"variant"];
    
    //forwardVariants = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in variants0) {
        
        CISVariant *variant = [[CISVariant alloc] initVariantWidthDataFromDictionary:dict];
        [variants addObject:variant];
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////
    // create backward flight varants (backwardVariants)
    
    NSDictionary *dir1 = [legs valueForKey:@"dir1"];
    NSArray *variants1 = [dir1 valueForKey:@"variant"];
    
    //backwordVariants = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in variants1) {
        
        CISVariant *variant = [[CISVariant alloc] initVariantWidthDataFromDictionary:dict];
        [variants addObject:variant];
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:NO];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [variants count];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    if (section == 0) {
        return @"Туда";
    }
    
    if (section == 1) {
        
        if ([variants count] == 2) {
            return @"Обратно";
        } else {
            return [NSString stringWithFormat:@"Обратно %ld варианта", [variants count] - 1];
        }
    }
    
    return @"";
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    CISVariant *variant = [variants objectAtIndex:section];
    return [variant.flights count] + 1 + 3;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CISVariant *variant = [variants objectAtIndex:indexPath.section];
    
    NSString *cellIdentifier = nil;
    
    if (indexPath.row == 0) {
        cellIdentifier = @"cellPlaneIconForward";
    }
    
    if (indexPath.row == 1) {
        cellIdentifier = @"cellAirLineNameAndFlightNumber";
    }
    
    if (indexPath.row >= 2 && indexPath.row <= ([variant.flights count] + 2)) {
        cellIdentifier = @"cellTrackPoint";
    }
    
    if (indexPath.row == ([variant.flights count] + 3)) {
        cellIdentifier = @"cellTravelTime";
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    if (cellIdentifier == nil) {
        cellIdentifier = @"cell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    if ([cellIdentifier isEqualToString:@"cellPlaneIconForward"]) {
        ((UILabel *)[cell viewWithTag:1]).text = [NSString stringWithFormat:@"%@р.", [self priceToSeporatedString: travelPrice]];;
    }
    
    if ([cellIdentifier isEqualToString:@"cellAirLineNameAndFlightNumber"]) {
        
        CISFlight *firstFlight = [variant.flights firstObject];
        
        ((UILabel *)[cell viewWithTag:1]).text = firstFlight.airLineName;
        ((UILabel *)[cell viewWithTag:2]).text = [NSString stringWithFormat:@"Рейс: %@", firstFlight.flightNumber];
    }
    
    if ([cellIdentifier isEqualToString:@"cellTrackPoint"]) {
        
        NSArray *flights = variant.flights;
        
        NSInteger pointIndex = indexPath.row - 2;
        
        if (pointIndex == 0) {
            
            CISFlight *flight = [flights firstObject];
            
            ((UILabel *)[cell viewWithTag:1]).text = flight.fromCode;
            ((UILabel *)[cell viewWithTag:2]).text = flight.fromName;
            ((UILabel *)[cell viewWithTag:3]).text = [city_from valueForKey:@"name"];
            
            ((UILabel *)[cell viewWithTag:4]).text = flight.timeFrom;
            ((UILabel *)[cell viewWithTag:5]).text = flight.dateFrom;
            ((UILabel *)[cell viewWithTag:6]).text = flight.dayOfWeekFrom;

            
        } else if (pointIndex < [flights count]) {
            
            CISFlight *prevFlight = [flights objectAtIndex:pointIndex - 1];
            CISFlight *flight = [flights objectAtIndex:pointIndex];
            
            ((UILabel *)[cell viewWithTag:1]).text = @"";
            ((UILabel *)[cell viewWithTag:2]).text = flight.fromName;
            ((UILabel *)[cell viewWithTag:3]).text = @"";
            
            ((UILabel *)[cell viewWithTag:4]).text = @"";
            ((UILabel *)[cell viewWithTag:5]).text = [NSString stringWithFormat:@"%@ - %@", prevFlight.timeTo, flight.timeFrom];
            ((UILabel *)[cell viewWithTag:6]).text = @"";

            
        } else {
            
            CISFlight *flight = [flights lastObject];
            
            ((UILabel *)[cell viewWithTag:1]).text = flight.toCode;
            ((UILabel *)[cell viewWithTag:2]).text = flight.toName;
            ((UILabel *)[cell viewWithTag:3]).text = [NSString stringWithFormat:@"%@, %@", [city_to valueForKey:@"name"], [country_to valueForKey:@"name"]];
            
            ((UILabel *)[cell viewWithTag:4]).text = flight.timeTo;
            ((UILabel *)[cell viewWithTag:5]).text = flight.dateTo;
            ((UILabel *)[cell viewWithTag:6]).text = flight.dayOfWeekTo;

        }
        
    }
    
    if ([cellIdentifier isEqualToString:@"cellTravelTime"]) {
        //((UILabel *)[cell viewWithTag:1]).text = [NSString stringWithFormat:@"Время в пути: %@", forwardFlight.flightDuration];
    }
    
    return cell;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CISVariant *variant = [variants objectAtIndex:indexPath.section];
    
    CGFloat ret = 38.0;
    
    if (indexPath.row >= 2 && indexPath.row <= ([variant.flights count] + 2)) {
            ret = 75.0;
    }
    
    return ret;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (section < 2) {
        return 40.0;
    } else {
        return 10.0;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)priceToSeporatedString:(NSString *)str
{
    
    NSMutableString *ret = [[NSMutableString alloc] init];
    NSMutableString *cut = [NSMutableString stringWithFormat:@"%ld", (long)[str integerValue]];
    
    while ([cut length] > 3) {
        
        NSString *last3ch = [cut substringFromIndex:[cut length] - 3];
        ret = [NSMutableString stringWithFormat:@"%@ %@", last3ch, ret];
        
        cut = [NSMutableString stringWithString:[cut substringToIndex:[cut length] - 3]];
        
    }
    
    if ([cut length] > 0) {
        
        ret = [NSMutableString stringWithFormat:@"%@ %@", cut, ret];
        
    }
    
    return ret;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------


@end
