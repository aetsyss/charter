//
//  CISHTTP.m
//  keepConnection
//
//  Created by Алексей Цысс on 21.05.14.
//  Copyright (c) 2014 ittaxi. All rights reserved.
//

#import "CISHTTP.h"

@implementation CISHTTP
{
    NSURL *serverURL;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

+ (CISHTTP *)shared
{
    static dispatch_once_t once = 0;
	static CISHTTP *mainConnection;
    
    dispatch_once(&once, ^{ mainConnection = [[CISHTTP alloc] init]; });
	
	return mainConnection;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (id)init
{
    self = [super init];
    
    if (self) {
     
        serverURL = [NSURL URLWithString:@""];
        
	}
    
	return self;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)initWithServerURL:(NSString *)urlString
{
   
    serverURL = [NSURL URLWithString:urlString];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

static NSString *toString(id object)
{
    return [NSString stringWithFormat: @"%@", object];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

static NSString *urlEncode(id object)
{
    NSString *string = toString(object);
    return [string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)urlEncodedStringFromDictionary:(NSDictionary *)dict
{
    NSMutableArray *parts = [NSMutableArray array];
    
    for (id key in dict) {
        id value = [dict objectForKey: key];
        NSString *part = [NSString stringWithFormat: @"%@=%@", urlEncode(key), urlEncode(value)];
        [parts addObject: part];
    }
    
    //[parts addObject:@"clientId=1"];
    [parts addObject:@"clientId=643"];
    //[parts addObject:@"key=6BA95EEC-9E62-4845-9791-3F8C097F6A99"];
    [parts addObject:@"key=0E982C89-CE3F-489B-8111-BCE7C6500BFA"];
    [parts addObject:@"token="];
    
    return [parts componentsJoinedByString: @"&"];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)postData:(NSDictionary *)jsonData withCommand:(NSString *)command completionHandler:(void (^)(NSDictionary *))myCompletionHandler
{
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[serverURL URLByAppendingPathComponent:command]];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:20];
    
    NSString *requestType = [jsonData valueForKey:@"type"];
    NSString *myParameters = [self urlEncodedStringFromDictionary:jsonData];
    NSData *postData = [myParameters dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:postData completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            NSString *errorMessage = [NSString stringWithFormat:@"httpError: %@", error.description];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"error: %@", errorMessage);
                [ProgressHUD showError:errorMessage];
                
            });
            
        } else {
            
            NSError *error;
            NSDictionary *ans = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            NSDictionary *errorFromServer = [ans valueForKey:@"error"];
            
            if (errorFromServer != nil) {
                
                NSString *comment = [errorFromServer valueForKey:@"comment"];
                NSNumber *errorNumber = [errorFromServer valueForKey:@"number"];
                
                NSString *errorMessage = [NSString stringWithFormat:@"errorFromServer: %@: %@, %@", requestType, errorNumber, comment];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSLog(@"error: %@", errorMessage);
                    [ProgressHUD showError:errorMessage];
                    
                });
                
            } else {
                
                if (ans != nil) {
                    
                    myCompletionHandler(ans);

                } else {
                    
                    NSLog(@"HTTP Post ans is nil");
                }
 
            }
            
        }
        
    }];
    
    [task resume];
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

@end
