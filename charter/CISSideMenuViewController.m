//
//  CISSideMenuViewController.m
//  charter
//
//  Created by Алексей Цысс on 04.08.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import "CISSideMenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface CISSideMenuViewController ()
{
    NSMutableArray *tableData;
    NSMutableArray *filterData;
    
    BOOL showFromCities, showToCities, showWhere, showPassangers;
}

@end

@implementation CISSideMenuViewController

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _mainTable.separatorColor = [UIColor clearColor];
    
    tableData = [[NSMutableArray alloc] init];
    //[tableData addObject:@"ВСЕ ПРЕДЛОЖЕНИЯ"];
    //[tableData addObject:@"СПЕЦ. ПРЕДЛОЖЕНИЯ"];
    
    filterData = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSсhedCountryList) name:@"getSсhedCountryList" object:nil];

    
    // Do any additional setup after loading the view.
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated
{
   
    NSIndexPath *sel = [_mainTable indexPathForSelectedRow];
    [_mainTable deselectRowAtIndexPath:sel animated:YES];
    
    showFromCities = NO;
    showToCities = NO;
    showWhere = NO;
    showPassangers = NO;

    [self makeFilterData];
    
    [_mainTable reloadData];

}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)getSсhedCountryList
{
    
    showToCities = YES;
    [self makeFilterData];
    [_mainTable reloadData];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)makeFilterData
{
 
    [filterData removeAllObjects];
    
    [filterData addObject:@{@"type": @1, @"title": @"Откуда:", @"detail": [CISGlobalData shared].selectedCityFrom, @"height": @44}];
    
    if (showFromCities) {
        
        //[filterData addObject:@{@"type": @310, @"title": @"", @"detail": @"любой город", @"height": @30}];

        for (NSDictionary *dict in [CISGlobalData shared].cityFromList) {
            NSString *cityName = [dict valueForKey:@"cityName"];
            NSString *counryName = [dict valueForKey:@"countryName"];
            
            [filterData addObject:@{@"type": @31, @"title": counryName, @"detail": cityName, @"height": @30, @"data": dict}];

        }

    }
    
    [filterData addObject:@{@"type": @2, @"title": @"Куда:", @"detail": [CISGlobalData shared].selectedCityTo, @"height": @44}];
    
    if (showToCities) {
        
        [filterData addObject:@{@"type": @320, @"title": @"", @"detail": @"любой город", @"height": @30}];
        
        for (NSDictionary *dict in [CISGlobalData shared].cityToList) {
            NSString *cityName = [dict valueForKey:@"cityName"];
            NSString *counryName = [dict valueForKey:@"countryName"];

            [filterData addObject:@{@"type": @32, @"title": counryName, @"detail": cityName, @"height": @30, @"data": dict}];
            
        }
    }
    
    [filterData addObject:@{@"type": @4, @"title": @"Когда:", @"detail": [CISGlobalData shared].filterWhenString, @"height": @44}];
    
    if (showWhere) {
        
        [filterData addObjectsFromArray:[self getWhereList]];
        
    }
    
    [filterData addObject:@{@"type": @5, @"title": @"Людей:", @"detail": [CISGlobalData shared].passangersCount, @"height": @44}];
    
    if (showPassangers) {
        [filterData addObject:@{@"type": @51, @"title": @"", @"detail": @"1 человек", @"height": @30, @"data": @"1"}];
        [filterData addObject:@{@"type": @51, @"title": @"", @"detail": @"2 человека", @"height": @30, @"data": @"2"}];
        [filterData addObject:@{@"type": @51, @"title": @"", @"detail": @"3 человека", @"height": @30, @"data": @"3"}];
        [filterData addObject:@{@"type": @51, @"title": @"", @"detail": @"4 человека", @"height": @30, @"data": @"4"}];
        [filterData addObject:@{@"type": @51, @"title": @"", @"detail": @"5 человек", @"height": @30, @"data": @"5"}];
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 4;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    NSArray *arr = [NSArray arrayWithObjects:@"", @"Фильтр", @"Подписка на уведомления", @"Личные данные", nil];
    
    return [arr objectAtIndex:section];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return [tableData count];
    } else if (section == 1) {
        return [filterData count];
    } else if (section == 2) {
        return 3;
    } else {
        return 2;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = nil;
        
    if (indexPath.section == 0) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];;
        
        cell.textLabel.text = [tableData objectAtIndex:indexPath.row];

    } else if (indexPath.section == 1) {
        
        NSDictionary *dict = [filterData objectAtIndex:indexPath.row];
        
        NSNumber *type = [dict valueForKey:@"type"];
        
        NSString *title = [dict valueForKey:@"title"];
        NSString *detail = [dict valueForKey:@"detail"];
        
        if ([type isEqualToNumber:@31] || [type isEqualToNumber:@32] || [type isEqualToNumber:@40] || [type isEqualToNumber:@310] || [type isEqualToNumber:@320] || [type isEqualToNumber:@51]) {
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"cellCity" forIndexPath:indexPath];
            
            if ([[CISGlobalData shared].selectedCityFrom isEqualToString:detail] || [[CISGlobalData shared].selectedCityTo isEqualToString:detail]) {
                [((UILabel *)[cell viewWithTag:2]) setTextColor:[UIColor redColor]];
            } else {
                [((UILabel *)[cell viewWithTag:2]) setTextColor:[UIColor colorWithRed:(217.0 / 255.0) green:(218.0 / 255.0) blue:(217.0 / 255.0) alpha:1.0]];

            }
            
        } else {
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"cellSearch" forIndexPath:indexPath];
            
            if (([type isEqualToNumber:@2] || [type isEqualToNumber:@1]) && [detail isEqualToString:@""]) {
                detail = @"любой город";
            }

        }
        
        ((UILabel *)[cell viewWithTag:1]).text = title;
        ((UILabel *)[cell viewWithTag:2]).text = detail;
        
    } else if (indexPath.section == 2) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];;
        
        if (indexPath.row == 0) {
            cell.textLabel.text = @"СПб - Рим, ниже 5 000 р.";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"СПб - все города, ниже 3 000 р.";
        } else {
            cell.textLabel.text = @"управление подписками...";
            cell.textLabel.alpha = 0.3;
        }
    
    } else {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"cellPersonal" forIndexPath:indexPath];;

        if (indexPath.row == 0) {
            cell.textLabel.text = @"Цысс Алексей Евгеньевич";
            cell.detailTextLabel.text = @"10 03 610847 до 22.08.2020";
        } else {
            cell.textLabel.text = @"Брусенко Анастасия Сергеевна";
            cell.detailTextLabel.text = @"12 14 456235 до 14.10.2026";
        }
    }
    
    
    return cell;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat ret = 44.0;
    
    if (indexPath.section == 1) {
     
        NSDictionary *dict = [filterData objectAtIndex:indexPath.row];
        NSNumber *h = [dict valueForKey:@"height"];
       
        ret = [h floatValue];
    }
    
    return ret;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSIndexPath *sel = [tableView indexPathForSelectedRow];
    [tableView deselectRowAtIndexPath:sel animated:YES];
    
    if (indexPath.section == 0) {
        
        [self.slidingViewController resetTopViewAnimated:YES];

    } else if (indexPath.section == 1) {
        
        NSDictionary *dict = [filterData objectAtIndex:indexPath.row];
        NSNumber *type = [dict valueForKey:@"type"];
        
        if ([type isEqualToNumber:@1]) {
            
            showFromCities = !showFromCities;
            [self makeFilterData];
            [_mainTable reloadData];
        }
        
        if ([type isEqualToNumber:@2]) {
            
            showToCities = !showToCities;
            [self makeFilterData];
            [_mainTable reloadData];
            
            /*if (showToCities == NO) {
                
                showToCities = YES;
                [self makeFilterData];
                [_mainTable reloadData];
                
            } else {
                
                showToCities = NO;
                [self makeFilterData];
                [_mainTable reloadData];
            }*/
            
        }
        
        if ([type isEqualToNumber:@31]) {
            
            showFromCities = NO;
            NSDictionary *data = [dict valueForKey:@"data"];
            
            [CISGlobalData shared].cityFromData = data;
            [CISGlobalData shared].selectedCityFrom = [NSMutableString stringWithString:[data valueForKey:@"cityName"]];
            
            [CISGlobalData shared].selectedCityTo = [NSMutableString stringWithString:@""];
            [CISGlobalData shared].cityToData = nil;
            
            [self makeFilterData];
            [_mainTable reloadData];
            
            [[CISGlobalData shared] getSchedCountryListFromCountry:[data valueForKey:@"countryCode"] andFromCity:[data valueForKey:@"cityCode"]];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTicketsTable" object:nil];
            [self.slidingViewController resetTopViewAnimated:YES];

        }
        
        if ([type isEqualToNumber:@310]) {
            
            showFromCities = NO;
            
            [CISGlobalData shared].cityFromData = nil;
            [CISGlobalData shared].selectedCityFrom = [NSMutableString stringWithString:@""];
            
            [CISGlobalData shared].selectedCityTo = [NSMutableString stringWithString:@""];
            [CISGlobalData shared].cityToData = nil;
            
            [self makeFilterData];
            [_mainTable reloadData];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTicketsTable" object:nil];
            [self.slidingViewController resetTopViewAnimated:YES];
            
        }
        
        if ([type isEqualToNumber:@32]) {
            
            showToCities = NO;
            NSDictionary *data = [dict valueForKey:@"data"];
            
            [CISGlobalData shared].selectedCityTo = [NSMutableString stringWithString:[data valueForKey:@"cityName"]];
            [CISGlobalData shared].cityToData = data;
            
            [self makeFilterData];
            [_mainTable reloadData];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTicketsTable" object:nil];
            [self.slidingViewController resetTopViewAnimated:YES];
        }
        
        if ([type isEqualToNumber:@320]) {
            
            showToCities = NO;
            
            [CISGlobalData shared].selectedCityTo = [NSMutableString stringWithString:@""];
            [CISGlobalData shared].cityToData = nil;
            
            [self makeFilterData];
            [_mainTable reloadData];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTicketsTable" object:nil];
            [self.slidingViewController resetTopViewAnimated:YES];
        }
        
        if ([type isEqualToNumber:@4]) {
            
            showWhere = !showWhere;
            [self makeFilterData];
            [_mainTable reloadData];
        }
        
        if ([type isEqualToNumber:@40]) {
        
            NSDictionary *data = [dict valueForKey:@"data"];
            NSString *detail = [dict valueForKey:@"detail"];
            
            [CISGlobalData shared].filterWhen = data;
            [CISGlobalData shared].filterWhenString = [NSMutableString stringWithString:detail];
            
            showWhere = NO;
            [self makeFilterData];
            [_mainTable reloadData];
            
            [self.slidingViewController resetTopViewAnimated:YES];
        }
        
        if ([type isEqualToNumber:@5]) {
            
            showPassangers = !showPassangers;
            [self makeFilterData];
            [_mainTable reloadData];
        }
        
        if ([type isEqualToNumber:@51]) {
            
            showPassangers = NO;
            
            NSString *data = [dict valueForKey:@"data"];
            [CISGlobalData shared].passangersCount = [NSMutableString stringWithString:data];
            
            [self makeFilterData];
            [_mainTable reloadData];
                        
            [self.slidingViewController resetTopViewAnimated:YES];
        }

    }
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSArray *)getWhereList
{

    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    NSArray *months = [NSArray arrayWithObjects:@"январь", @"февраль", @"март", @"апрель", @"май", @"июнь", @"июль", @"август", @"сентябрь", @"октябрь", @"ноябрь", @"декабрь", nil];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    
    [ret addObject:@{@"type": @40, @"title": @"", @"detail": @"ближайшие полгода", @"height": @30}];
    
    for (int j=0; j<6; j++) {
        
        if (month + j > 12) {
            month = -j + 1;
            year += 1;
        }
        
        NSString *str = [NSString stringWithFormat:@"%@, %ld", [months objectAtIndex:month + j - 1], (long)year];
        NSDictionary *data = @{@"month": [NSNumber numberWithInt:((int)month + j)], @"year": [NSNumber numberWithInteger:year]};
        
        [ret addObject:@{@"type": @40, @"title": @"", @"detail": str, @"height": @30, @"data": data}];
        
    }
    
    return ret;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------

@end
