//
//  CISFlight.h
//  charter
//
//  Created by Алексей Цысс on 07.08.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CISFlight : NSObject

@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *flightNumber;

@property (strong, nonatomic) NSString *airLineName;
@property (strong, nonatomic) NSString *airLineCode;

@property (strong, nonatomic) NSString *dateFrom;
@property (strong, nonatomic) NSString *dateTo;

@property (strong, nonatomic) NSString *dayOfWeekFrom;
@property (strong, nonatomic) NSString *dayOfWeekTo;

@property (strong, nonatomic) NSString *fromCode;
@property (strong, nonatomic) NSString *fromName;

@property (strong, nonatomic) NSString *toCode;
@property (strong, nonatomic) NSString *toName;

@property (strong, nonatomic) NSString *timeFrom;
@property (strong, nonatomic) NSString *timeTo;
@property (strong, nonatomic) NSString *flightDuration;

- (id)initWithFlightDictionary:(NSDictionary *)flight;

- (NSDate *)fromDate;
- (NSDate *)toDate111;

@end
