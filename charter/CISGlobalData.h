//
//  CISGlobalData.h
//  charter
//
//  Created by Алексей Цысс on 24.07.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CISHTTP.h"

@interface CISGlobalData : NSObject

+ (CISGlobalData *)shared;

@property (strong, nonatomic) NSMutableDictionary *countryList;
@property (strong, nonatomic) NSMutableArray *cityFromList;
@property (strong, nonatomic) NSMutableArray *cityToList;
@property (strong, nonatomic) NSMutableArray *specialList;
@property (strong, nonatomic) NSMutableArray *tariffList;


//// search settings ////////////////////////////////////////////
@property (strong, nonatomic) NSMutableString *selectedCityFrom;
@property (strong, nonatomic) NSMutableString *selectedCityTo;

@property (strong, nonatomic) NSDictionary *cityFromData;
@property (strong, nonatomic) NSDictionary *cityToData;

@property (strong, nonatomic) NSDictionary *filterWhen;
@property (strong, nonatomic) NSMutableString *filterWhenString;

@property (strong, nonatomic) NSMutableString *passangersCount;

@property (strong, nonatomic) NSMutableString *dateFrom;
@property (strong, nonatomic) NSMutableString *dateTo;
/////////////////////////////////////////////////////////////////

- (void)getCountryList;
- (void)getCityFromList;
- (void)getSchedCountryListFromCountry:(NSString *)country andFromCity:(NSString *)city;
- (void)getTariffList;

- (void)getSpecialListFromCountry:(NSString *)country_from fromCity:(NSString *)city_from toCountry:(NSString *)country_to toCity:(NSString *)city_to;

- (NSString *)rotateDate:(NSString *)date;
- (NSDictionary *)getCityToDictionaryByCityName:(NSString *)city_name;

@end
